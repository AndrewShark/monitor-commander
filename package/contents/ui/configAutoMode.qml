import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.kde.plasma.core 2.0
import org.kde.plasma.plasmoid 2.0

Kirigami.FormLayout {
    id: autoroot
    anchors.left: parent.left
    anchors.right: parent.right

    Item {
        Kirigami.FormData.label: "Assign a preset mode to specified application windows."
        Kirigami.FormData.isSection: true
    }

    Label {
        text: "usagetip under a set of radio buttons when Touch Mode is on", "Automatically set to Large when in Touch Mode"
        font: Kirigami.Theme.smallFont
    }

    
    //RowLayout {
        ScrollView {
            id: scrollView
            height: 100
            width: 500
            
            ListView {
                height: 500
                model: 5
                delegate:      TextField {
                    Kirigami.FormData.label: i18n("Label:")
                    placeholderText: i18n("Placeholder")
                    width: 500
                }
            }
        }    
    //}

    RowLayout {
        Button {
            text: "Create"
            Layout.alignment: Qt.AlignLeft
//             onClicked: model.submit()
        }

        Button {
            text: "Import"
//             onClicked: model.revert()
        }
        Button {
            text: "Export"
//             onClicked: model.revert()
        }
    }
    
    // FileDialogLoader {
    //     id: exportDialog
    //     title: i18n("Export Rules")
    //     isSaveDialog: true
    //     //onLastFolderChanged: {
    //         //importDialog.lastFolder = lastFolder;
    //     //}
    //     //onFileSelected: {
    //         //selectedIndexes.sort();
    //         //kcm.exportToFile(path, selectedIndexes);
    //         //exportInfo.visible = false;
    //     //}
    // }
    
    //RowLayout {
        //Kirigami.FormData.label: i18n("Sound File:")
        
        //TextField {
            //id: variableName
            //placeholderText: i18n("No file selected.")
        //}
        //Button {
            //text: i18n("Browse")
            //icon.name: "folder-symbolic"
            //onClicked: fileDialogLoader.active = true
            
            //Loader {
                //id: fileDialogLoader
                //active: false
                
                //sourceComponent: FileDialog {
                    //id: fileDialog
                    //folder: shortcuts.music
                    //nameFilters: [
                    //i18n("Sound files (%1)", "*.wav *.mp3 *.oga *.ogg"),
                    //i18n("All files (%1)", "*"),
                    //]
                    //onAccepted: {
                        //variableName.text = fileUrl
                        //fileDialogLoader.active = false
                    //}
                    //onRejected: {
                        //fileDialogLoader.active = false
                    //}
                    //Component.onCompleted: open()
                //}
            //}
        //}
    //}

}

