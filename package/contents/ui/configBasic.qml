// configGeneral.qml
import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.0
import org.kde.kirigami 2.4 as Kirigami

Kirigami.FormLayout {
    id: page
    property alias cfg_variableName: variableName.checked

    CheckBox {
        id: variableName
        Kirigami.FormData.label: i18n("Icon:")
        text: i18n("Show")
    }
}
